# SPDX-FileCopyrightText: 2024 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

import json
from pathlib import Path

import pytest

from pglift_cli import _site


def test_site_settings(monkeypatch: pytest.MonkeyPatch, tmp_path: Path) -> None:
    with monkeypatch.context() as m:
        m.setenv("SETTINGS", json.dumps({"prefix": str(tmp_path)}))
        _site._settings.cache_clear()
        s = _site.SETTINGS
        assert s.prefix == tmp_path

        m.setenv("SETTINGS", json.dumps({"prefix": str(tmp_path / "foo")}))
        _site._settings.cache_clear()
        s = _site.SETTINGS
        assert s.prefix == tmp_path / "foo"
