# SPDX-FileCopyrightText: 2024 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

from pathlib import Path

from pglift_cli._settings import Settings


def test_settings(tmp_path: Path, bindir_template: str) -> None:
    s = Settings.model_validate(
        dict(prefix="/", postgresql={"bindir": bindir_template})
    )
    assert s.cli.logpath == Path("/log")
