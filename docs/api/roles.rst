.. SPDX-FileCopyrightText: 2021 Dalibo
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Roles
=====

.. currentmodule:: pglift.roles

Module :mod:`pglift.roles` exposes the following API to manipulate
PostgreSQL roles:

.. autofunction:: apply
.. autofunction:: exists
.. autofunction:: get
.. autofunction:: drop
