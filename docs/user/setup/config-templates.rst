.. SPDX-FileCopyrightText: 2021 Dalibo
..
.. SPDX-License-Identifier: GPL-3.0-or-later

.. _configuration_templates:

Configuration templates
=======================

Instance and satellite components configuration is built from site-wise
configuration files.

These files are templates which means that values in curly braces
``{<placeholder>}`` are substituted by site settings values.

These templates can be overridden by providing them in either
``$PGLIFT_CONFIG_DIR``, ``$XDG_CONFIG_HOME/pglift`` [#xdgconfighome]_ or
``/etc/pglift`` (by order of precedence).

PostgreSQL templates
--------------------

Since pglift is able to manage different versions of PostgreSQL, site
administrators can install templates for PostgreSQL configuration files
(``postgresql.conf``, ``pg_hba.conf``, etc.) with a *version* path component.
For example, if the file ``postgresql/16/postgresql.conf`` is found in site
configuration directory and an instance with version *16* is being created,
this file will be used as a template for initializing the ``postgresql.conf``
of this new instance. If, on the other hand, an instance with another version,
say *14*, is being created but no ``postgresql/14/postgresql.conf`` file is
found in site configuration directory, the default
``postgresql/postgresql.conf`` template file (either found in site
configuration or in pglift distribution) will be used.

.. [#xdgconfighome]
   Where ``$XDG_CONFIG_HOME`` would be ``$HOME/.config`` unless configured
   differently.
