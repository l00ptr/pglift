.. SPDX-FileCopyrightText: 2024 Dalibo
..
.. SPDX-License-Identifier: GPL-3.0-or-later

.. _cli_logging:

Logging in the command-line interface
=====================================

When invoked without any specific option, any ``pglift`` command will log
messages to the standard error in form of ``<LEVEL> <message>`` lines. The
default threshold for displayed messages is ``INFO`` so that messages with
this severity or higher are displayed but those with a lower severity (like
``DEBUG`` messages) are not. To control this threshold, the ``-L,
--log-level`` should be used.

To log messages to a file, the ``-l, --log-file LOGFILE`` should be used.

When an unexpected error occurs during the invocation of a pglift command
(i.e. something that is not a user error but may be a bug), pglift will write
all messages logged during this invocation to a file and finally display its
path in standard error:

.. code-block:: console

    $ pglift ...
    ERROR    an unexpected error occurred
             Traceback (most recent call last):
               ...
             OSError: ...
    Error: an unexpected error occurred, this is probably a bug;
        details can be found at /tmp/pglift/1714116611.5114818.log

The directory path where this log file would be written can be configured
through the ``cli.logpath`` setting (set to ``/tmp/pglift`` in the above
example). If, for some reason, writing to this directory is not possible, a
temporary directory will be used instead as a best effort to still save
execution details.

This log file is meant for troubleshooting and will not persist if the command
exits in an expected way (which may be a success and a known/user error). So
users willing to keep a log of pglift command invocations should use the ``-l,
--log-file`` option mentioned above.
