<!--
SPDX-FileCopyrightText: 2024 Dalibo

SPDX-License-Identifier: GPL-3.0-or-later
-->

pglift is a solution aiming at deploying production-ready instances of
PostgreSQL, set up, backed up and monitored.

This package contains the library code holding all the business logic.

The pglift project also comes with a command-line interface, to be installed through the
`cli` optional dependency (e.g. `pip install "pglift[cli]"`), and an Ansible
collection.

See the project [documentation](https://pglift.readthedocs.io/) for
installation and tutorials.
