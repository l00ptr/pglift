# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

---
include:
  - template: 'Workflows/Branch-Pipelines.gitlab-ci.yml'

variables:
  GIT_FETCH_EXTRA_FLAGS: --tags
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  PYOXIDIZER_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pyoxidizer"
  PGLIFT_BINARY_URL: "$CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/generic/bin/$CI_COMMIT_REF_NAME/pglift.tar.gz"
  PGLIFT_BINARY_ARTIFACT: "pyoxidizer/build/x86_64-unknown-linux-gnu/release/install"

default:
  interruptible: true

stages:
  - build
  - check
  - test
  - publish

checks:
  stage: check
  except:
    - tags
  image: $CI_REGISTRY_IMAGE:py$pyversion
  before_script:
    - git config --global --add safe.directory "$CI_PROJECT_DIR"
  script:
    - tox -e lint
    - tox -e lint-ansible
    - tox -e typing
    - tox -e tests-doctest
    - tox -e tests-doctest_ansible
    - tox -e tests-unit
    - tox -e docs
    - tox -e bandit
  parallel:
    matrix:
      - pyversion: "3.9"
      - pyversion: "3.11"
  cache:
    paths:
      - .cache/pip
  artifacts:
    paths: [docs/_build/]
    expire_in: 3 days

buildbin:
  stage: test
  rules:
    - if: '$CI_PROJECT_ROOT_NAMESPACE == "dalibo" && $CI_COMMIT_TAG =~ /^v.*/'
      when: always
      allow_failure: true
    - changes:
        - pyoxidizer/requirements.txt
      when: always
    - when: manual
      allow_failure: true
  image: $CI_REGISTRY_IMAGE:rockylinux8_buildbin
  script:
    - tox -e buildbin
  cache:
    paths:
      - .cache/pip
      - .cache/pyoxidizer
  artifacts:
    paths:
      - "$PGLIFT_BINARY_ARTIFACT/*"
    expire_in: 1 day

tests:
  stage: test
  except:
    - tags
  image: $CI_REGISTRY_IMAGE:$dist
  script:
    - tox -e tests-func -- $opts -x -o faulthandler_timeout=600
    - tox -e tests-expect
    - tox -e tests-ansible
  parallel:
    matrix:
      - dist: debian
        opts: --pg-auth peer --pgbackrest-repo-host=ssh --surole-name asurole1
      - dist: rockylinux8
        opts: --pg-auth pgpass --pgbackrest-repo-host=tls
      - dist: rockylinux9
        opts: --pg-auth password_command
  cache:
    paths:
      - .cache/pip

tests-binary:
  stage: test
  image: $CI_REGISTRY_IMAGE:$dist
  needs: ['buildbin']
  rules:
    - if: '$CI_PROJECT_ROOT_NAMESPACE == "dalibo" && $CI_COMMIT_TAG =~ /^v.*/'
      when: always
      allow_failure: true
    - when: manual
      allow_failure: true
  script:
    - tox -e tests-binary
  parallel:
    matrix:
      - dist: debian
      - dist: rockylinux8
      - dist: rockylinux9
  cache:
    paths:
      - .cache/pip

.ci_image:
  stage: build
  rules:
    - if: '$CI_PROJECT_ROOT_NAMESPACE == "dalibo" && $CI_COMMIT_TAG =~ /^v.*/'
      when: manual
      allow_failure: true
    - if: '($CI_PIPELINE_SOURCE == "schedule" && $CI_PROJECT_ROOT_NAMESPACE == "dalibo") || $CI_PROJECT_ROOT_NAMESPACE != "dalibo"'
      when: on_success
    - when: manual
      allow_failure: true
  image: docker:dind
  variables:
    BUILDOPTS: ""
  services:
    - docker:dind
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - cd ci/$WORKSUBDIR
    - docker pull $IMAGE_TAG || true
    - docker build --pull --cache-from $IMAGE_TAG --tag $IMAGE_TAG -f $DOCKERFILE $BUILDOPTS .
    - wget -O duuh https://github.com/philpep/duuh/releases/download/1.2/duuh-linux-amd64
    - chmod +x duuh
    - ./duuh -build $IMAGE_TAG
    - docker push $IMAGE_TAG

python_ci_image:
  extends: .ci_image
  variables:
    IMAGE_TAG: "$CI_REGISTRY_IMAGE:py$pyversion"
    WORKSUBDIR: "testing"
    DOCKERFILE: "Dockerfile.python"
    BUILDOPTS: "--build-arg version=$pyversion"
  parallel:
    matrix:
      - pyversion: "3.9"
      - pyversion: "3.11"

buildbin_ci_image:
  extends: .ci_image
  rules:
    - if: '$CI_PROJECT_ROOT_NAMESPACE == "dalibo" && $CI_COMMIT_TAG =~ /^v.*/'
      when: always
    - when: manual
      allow_failure: true
  variables:
    WORKSUBDIR: "buildbin"
    DOCKERFILE: "Dockerfile"
    IMAGE_TAG: "$CI_REGISTRY_IMAGE:rockylinux8_buildbin"

debian_ci_image:
  extends: .ci_image
  variables:
    IMAGE_TAG: "$CI_REGISTRY_IMAGE:debian"
    WORKSUBDIR: "testing"
    DOCKERFILE: "Dockerfile.debian"

rockylinux8_ci_image:
  extends: .ci_image
  variables:
    IMAGE_TAG: "$CI_REGISTRY_IMAGE:rockylinux8"
    WORKSUBDIR: "testing"
    DOCKERFILE: "Dockerfile.rockylinux8"

rockylinux9_ci_image:
  extends: .ci_image
  variables:
    IMAGE_TAG: "$CI_REGISTRY_IMAGE:rockylinux9"
    WORKSUBDIR: "testing"
    DOCKERFILE: "Dockerfile.rockylinux9"

.pypi-upload:
  rules:
    - if: '$CI_PROJECT_ROOT_NAMESPACE == "dalibo" && $CI_COMMIT_TAG =~ /^v.*/'
      when: always
    - when: never
  stage: publish
  image: python:3
  needs: []
  variables:
    TWINE_USERNAME: __token__
  before_script:
    - pip install twine
    - pip install build
  script:
    - cd $DIR
    - python3 -m build
    - python3 -m twine check dist/*
    - python3 -m twine upload -p $TWINE_PASSWORD dist/*
    - cd ..
  cache:
    paths:
      - .cache/pip

pypi-upload-lib:
  extends: .pypi-upload
  variables:
    DIR: lib
    TWINE_PASSWORD: $TWINE_PASSWORD_LIB

pypi-upload-cli:
  extends: .pypi-upload
  variables:
    DIR: cli
    TWINE_PASSWORD: $TWINE_PASSWORD_CLI

gitlab-package-upload:
  stage: publish
  rules:
    - if: '$CI_PROJECT_ROOT_NAMESPACE == "dalibo" && $CI_COMMIT_TAG =~ /^v.*/'
      when: always
    - when: manual
      allow_failure: true
  image: $CI_REGISTRY_IMAGE:rockylinux8_buildbin
  needs: ['buildbin']
  script:
    - cd "$PGLIFT_BINARY_ARTIFACT"
    - tar czf - * | curl --fail --header "JOB-TOKEN:$CI_JOB_TOKEN" --upload-file - "$PGLIFT_BINARY_URL?select=package_file"

ansible-collection-publish:
  stage: publish
  rules:
    - if: '$CI_PROJECT_ROOT_NAMESPACE == "dalibo" && $CI_COMMIT_TAG =~ /^ansible\/v.*/'
      when: always
    - when: never
  image: python:3
  needs: []
  before_script:
    - pip install ansible distlib
    - mkdir ~/.ansible/
    - 'echo "token: ${ANSIBLE_GALAXY_TOKEN}" > ~/.ansible/galaxy_token'
  script:
    - cd ansible
    - ansible-galaxy collection build
    - ansible-galaxy collection publish ./dalibo-pglift-${CI_COMMIT_REF_NAME:9}.tar.gz
