# SPDX-FileCopyrightText: 2024 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import Final

from rich.console import Console

console: Final = Console()
