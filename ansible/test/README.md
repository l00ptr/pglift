<!--
SPDX-FileCopyrightText: 2022 Dalibo

SPDX-License-Identifier: GPL-3.0-or-later
-->

# Pglift HA with Patroni for testing

```
$ docker-compose up -d
$ ansible-playbook -i inventory.py play_ha.yml
```
