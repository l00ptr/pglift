#!/bin/sh

# SPDX-FileCopyrightText: 2022 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

tail -F /var/lib/pglift/log/patroni/15-main/patroni.log &
exec rsyslogd -n
