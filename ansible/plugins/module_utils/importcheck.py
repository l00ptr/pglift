# SPDX-FileCopyrightText: 2022 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later


from __future__ import annotations

import sys
from collections.abc import Iterator
from contextlib import contextmanager

from ansible.module_utils.basic import missing_required_lib


@contextmanager
def check_required_libs() -> Iterator[None]:
    try:
        yield None
    except ImportError as e:
        print(missing_required_lib(e.name), file=sys.stderr)
        sys.exit(1)
