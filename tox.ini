# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

[tox]
minversion = 3.24.5
envlist = lint,lint-ansible,typing,tests-{doctest,doctest_ansible,unit,func,func_systemd,expect,ansible},bandit,docs
isolated_build = true

[testenv]
skip_install = true

[testenv:lint]
commands =
  black --check .
  flake8 .
  isort --check --diff .
  codespell
  reuse lint
  towncrier build --version=Unreleased --draft
  pre-commit run --all-files --show-diff-on-failure pyupgrade
deps =
  -r requirements/lint.txt

[testenv:lint-ansible]
changedir = ansible
commands =
  antsibull-changelog lint
deps =
  antsibull-changelog

[testenv:typing]
commands =
  mypy
deps =
  ansible-core
  -e ./lib[test,typing]
  -e ./cli[test]

[testenv:tests-{doctest,doctest_ansible,unit,func,func_systemd}]
commands =
  pytest {posargs:--showlocals}
changedir =
  doctest: lib/src
  doctest_ansible: ansible
  unit: tests/unit
  func: tests/func
  func_systemd: tests/func
deps =
  doctest: -e ./lib[test]
  doctest_ansible: ansible-core
  doctest_ansible: pytest
  unit: -e ./lib[test]
  unit: -e ./cli[test]
  func: -e ./lib[test]
  func_systemd: -e ./lib[test]
  func_systemd: -e ./cli
setenv =
  func: PYTHONASYNCIODEBUG=1
  func_systemd: PYTHONASYNCIODEBUG=1
passenv =
  func_systemd: DBUS_SESSION_BUS_ADDRESS
  func_systemd: XDG_RUNTIME_DIR

[testenv:tests-expect]
commands =
  prysk -v t
deps =
  -e ./lib
  -e ./cli[test]

[testenv:tests-ansible]
changedir =
  tests/ansible
commands =
  ansible-galaxy collection install ../../ansible
  pytest {posargs}
deps =
  -e ./lib
  -e ./cli
  ansible
  distlib
  patroni[etcd]>=2.1.5
  port-for
  psycopg
  psycopg2-binary
  PyYAML
  pytest
  temboard-agent>=8.1.0
  trustme
setenv =
  ANSIBLE_COLLECTIONS_PATH={toxworkdir}

[testenv:tests-binary]
changedir =
  tests/ansible
allowlist_externals =
  /usr/bin/ln
commands =
  ln -rs {toxinidir}/pyoxidizer/build/x86_64-unknown-linux-gnu/release/install/pglift {envbindir}
  pglift --version
  ansible-galaxy collection install ../../ansible
  pytest {posargs}
deps =
  ansible
  distlib
  patroni[etcd]>=2.1.5
  port-for
  psycopg
  psycopg2-binary
  PyYAML
  pytest
  tenacity
  temboard-agent>=8.1.0
  trustme
setenv =
  ANSIBLE_COLLECTIONS_PATH={toxworkdir}

[testenv:bandit]
commands =
  bandit -c {toxinidir}/.bandit -r {toxinidir}/lib/src {toxinidir}/cli/src {toxinidir}/ansible
deps =
  bandit

[testenv:docs]
commands =
  sphinx-build -b html -W -T docs docs/_build
deps =
  -r docs/requirements.txt
  -e ./lib

[testenv:pin]
commands =
  pip-compile \
    --upgrade \
    --output-file pyoxidizer/requirements.txt \
    lib/pyproject.toml cli/pyproject.toml
  - git commit -m "Update dependencies for PyOxidizer" pyoxidizer/requirements.txt
allowlist_externals =
  git
deps =
  pip-tools >= 7.0.0
passenv =
  EMAIL

[testenv:buildbin]
allowlist_externals =
  /usr/bin/ln
commands =
  pyoxidizer build --release --path pyoxidizer --system-rust
  ln -rs {toxinidir}/pyoxidizer/build/x86_64-unknown-linux-gnu/release/install/pglift {envbindir}
  pglift --version
deps =
  pyoxidizer
passenv =
  PYOXIDIZER_CACHE_DIR

[testenv:release-{lib,cli}]
changedir =
  lib: lib
  cli: cli
commands =
  git describe --exact-match
  rm -rf dist
  {envpython} -m build
  {envpython} -m twine check dist/*
  {envpython} -m twine upload --verbose dist/*
allowlist_externals =
  git
  rm
deps =
  build
  twine
setenv =
  TWINE_USERNAME=__token__
  TWINE_PASSWORD={env:PYPI_TOKEN}
