Restore ``database restore`` command. This command is only
available if ``postgresql.dump_commands`` as not been set in site settings.
