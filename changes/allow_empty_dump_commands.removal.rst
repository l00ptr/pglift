``postgresql.dump_commands`` setting now defaults to ``null``.
``database dump`` command is now built-in when this setting is left to its
default value. Overall behavior is unchanged.
