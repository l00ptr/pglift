Set schema owner to database owner if no owner for the schema got specified
but one was for the database. Previously, schema owner was set to the current
user in that case.
