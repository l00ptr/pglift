Document logging behaviours in the command-line interface in a new
:ref:`reference section <cli_logging>`.
